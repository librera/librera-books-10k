# librera-books-10k

This dataset contains ten thousand books from librera.pe 

Each book contains:
- ISBN 13
- title
- author
- description

The information has been obtained through web scraping from multiple book stores or publishers.

The format may not be 100% correct, but still valid for testing.

